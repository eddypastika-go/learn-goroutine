package learn_goroutine

import (
	"fmt"
	"sync"
	"testing"
	"time"
)

func TestPool(t *testing.T) {
	pool := sync.Pool{New: func() interface{} {
		return "This is default pool"
	}}

	pool.Put("Eddy")
	pool.Put("Pastika")
	pool.Put(60000)

	for i := 0; i < 10; i++ {
		go func() {
			//wg.Add(1)
			data := pool.Get()
			fmt.Println("Data: ", data)
			time.Sleep(time.Second)
			pool.Put(data)
			//wg.Done()
		}()
	}

	//wg.Wait()
	time.Sleep(5 * time.Second)
	fmt.Println("DONE!!!")
}
