package learn_goroutine

import (
	"fmt"
	"testing"
	"time"
)

func RunHelloWorld() {
	fmt.Println("Hello, World!")
}

func TestCreateGoroutine(t *testing.T) {
	go RunHelloWorld()
	fmt.Println("Doneeeee!!!")

	time.Sleep(time.Second)
}

func DisplayNumber(num int) {
	fmt.Println("Display", num)
}

func TestManyGoroutine(t *testing.T) {
	for i := 1; i <= 100000; i++ {
		go DisplayNumber(i)
	}

	time.Sleep(5 * time.Second)
}