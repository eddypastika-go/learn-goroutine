package learn_goroutine

import (
	"fmt"
	"sync"
	"testing"
	"time"
)

func TestTimer(t *testing.T) {
	timer := time.NewTimer(5 * time.Second)
	fmt.Println(time.Now())

	ti := <-timer.C
	fmt.Println(ti)
}

func TestAfter(t *testing.T) {
	ch := time.After(5 * time.Second)
	fmt.Println(time.Now())

	ti := <-ch
	fmt.Println(ti)
}

func TestAfterFunc(t *testing.T) {
	wgs := &sync.WaitGroup{}
	wgs.Add(1)

	time.AfterFunc(time.Second, func() {
		fmt.Println("executed after 1 sec!")
		wgs.Done()
	})

	wgs.Wait()
	fmt.Println("DONE!!!")
}
