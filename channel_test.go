package learn_goroutine

import (
	"fmt"
	"strconv"
	"testing"
	"time"
)

func TestCrateChannel(t *testing.T) {
	ch := make(chan string)
	defer close(ch)

	go func() {
		time.Sleep(2 * time.Second)
		ch <- "Eddy Pastika Putra"
		fmt.Println("channel is filled!")
	}()

	data := <-ch
	fmt.Println("Data: ", data)

	time.Sleep(time.Second)
}

func GiveMeResponse(ch chan string) {
	time.Sleep(2 * time.Second)
	ch <- "Eddy Pastika Putra"
	fmt.Println("channel is filled!")
}

func TestChannelAsParameter(t *testing.T) {
	ch := make(chan string)
	defer close(ch)

	go GiveMeResponse(ch)

	data := <-ch
	fmt.Println("Data: ", data)

	time.Sleep(time.Second)
}

func OnlyIn(ch chan<- string) {
	time.Sleep(2 * time.Second)
	ch <- "Eddy Pastika Putra"
	fmt.Println("channel IN")
}

func OnlyOut(ch <-chan string) {
	data := <-ch
	fmt.Println("Data: ", data)
	fmt.Println("channel OUT")
}

func TestInOutChannel(t *testing.T) {
	ch := make(chan string)
	defer close(ch)

	go OnlyIn(ch)
	go OnlyOut(ch)

	time.Sleep(2 * time.Second)
}

func TestBufferedChannel(t *testing.T) {
	ch := make(chan string, 3) // non-blocking
	defer close(ch)

	go func() {
		ch <- "Eddy"
		ch <- "Pastika"
		ch <- "Putra"
	}()

	go func() {
		fmt.Println(<-ch)
		fmt.Println(<-ch)
		fmt.Println(<-ch)
	}()

	time.Sleep(time.Second)
	fmt.Println("Done !!!")
}

func TestRangeChannel(t *testing.T) {
	ch := make(chan string)

	go func() {
		for i := 1; i <= 100000; i++ {
			ch <- "Number: " + strconv.Itoa(i)
		}
		close(ch)
	}()

	for data := range ch { // for 1 chan
		fmt.Println(data)
	}

	fmt.Println("DONE!!!!")
}

func TestSelectChannel(t *testing.T) {
	ch1 := make(chan string)
	ch2 := make(chan string)
	defer close(ch1)
	defer close(ch2)

	go GiveMeResponse(ch1)
	go GiveMeResponse(ch2)

	counter := 0
	for {
		select {
		case data := <-ch1:
			fmt.Println("Channel 1: ", data)
			counter++
		case data := <-ch2:
			fmt.Println("Channel 2: ", data)
			counter++
		}

		if counter == 2 {
			break
		}
	}
}

func TestSelectChannelWithDefault(t *testing.T) {
	ch1 := make(chan string)
	ch2 := make(chan string)
	defer close(ch1)
	defer close(ch2)

	go GiveMeResponse(ch1)
	go GiveMeResponse(ch2)

	counter := 0
	for {
		select {
		case data := <-ch1:
			fmt.Println("Channel 1: ", data)
			counter++
		case data := <-ch2:
			fmt.Println("Channel 2: ", data)
			counter++
		default:
			fmt.Println("waiting channel...")
		}

		if counter == 2 {
			break
		}
	}
}
