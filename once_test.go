package learn_goroutine

import (
	"fmt"
	"sync"
	"testing"
)

var counter int

func OnlyOnce() {
	counter++
}

func TestOnce(t *testing.T) {
	once := sync.Once{}
	wg := sync.WaitGroup{}

	for i := 0; i < 100; i++ {
		go func() {
			wg.Add(1)
			once.Do(OnlyOnce)
			wg.Done()
		}()
	}

	wg.Wait()
	fmt.Println("Counter: ", counter)
	fmt.Println("DONE!!!")
}
