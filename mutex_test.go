package learn_goroutine

import (
	"fmt"
	"sync"
	"testing"
	"time"
)

func TestMutex(t *testing.T) {
	var mutex sync.Mutex

	x := 0
	for i := 1; i <= 1000; i++ {
		go func() {
			for j := 1; j <= 100; j++ {
				mutex.Lock()
				x++
				mutex.Unlock()
			}
		}()
	}

	time.Sleep(5 * time.Second)
	fmt.Println("result: ", x)
}

type BankAccount struct {
	RWMutex sync.RWMutex
	Balance int
}

func (b *BankAccount) AddBalance(amount int) {
	b.RWMutex.Lock()
	b.Balance += amount
	b.RWMutex.Unlock()
}

func (b *BankAccount) GetBalance() int {
	b.RWMutex.RLock()
	bal := b.Balance
	b.RWMutex.RUnlock()

	return bal
}

func TestRWMutex(t *testing.T) {
	acc := BankAccount{}

	for i := 1; i <= 1000; i++ {
		go func() {
			for j := 1; j <= 100; j++ {
				acc.AddBalance(1)
				fmt.Println(acc.GetBalance())
			}
		}()
	}

	time.Sleep(5 * time.Second)
	fmt.Println("result: ", acc.GetBalance())
}
