package learn_goroutine

import (
	"fmt"
	"sync"
	"testing"
)

func AddToMap(wg *sync.WaitGroup, m *sync.Map, value int) {
	defer wg.Done()
	wg.Add(1)

	m.Store(value, value)
}

func TestMap(t *testing.T) {
	data := &sync.Map{}
	wg := &sync.WaitGroup{}

	for i := 0; i < 100; i++ {
		go AddToMap(wg, data, i)
	}

	wg.Wait()

	data.Range(func(key, value interface{}) bool {
		fmt.Println(key, ": ", value)
		return true
	})

	fmt.Println("DONE!!!")
}
