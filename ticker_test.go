package learn_goroutine

import (
	"fmt"
	"testing"
	"time"
)

func TestTicker(t *testing.T) {
	tick := time.NewTicker(time.Second)

	go func() {
		time.Sleep(5 * time.Second)
		tick.Stop()
	}()

	for data := range tick.C {
		fmt.Println(data)
	}
}

func TestTick(t *testing.T) {
	tick := time.Tick(time.Second)

	for data := range tick {
		fmt.Println(data)
	}
}
