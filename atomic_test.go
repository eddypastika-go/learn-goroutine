package learn_goroutine

import (
	"fmt"
	"sync"
	"sync/atomic"
	"testing"
)

func TestAtomic(t *testing.T) {
	var x int64 = 0
	wgs := &sync.WaitGroup{}

	for i := 1; i <= 1000; i++ {
		go func() {
			wgs.Add(1)
			for j := 1; j <= 100; j++ {
				//x++ // race cond
				atomic.AddInt64(&x, 1)
			}
			wgs.Done()
		}()
	}

	wgs.Wait()
	fmt.Println("result: ", x)
}
