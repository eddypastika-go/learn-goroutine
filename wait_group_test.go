package learn_goroutine

import (
	"fmt"
	"sync"
	"testing"
	"time"
)

func RunAsync(wg *sync.WaitGroup, n int) {
	defer wg.Done()
	wg.Add(1)
	fmt.Println("hello, ed!", n)

	time.Sleep(time.Second)
}

func TestWaitGroup(t *testing.T) {
	wg := &sync.WaitGroup{}

	for i := 0; i < 100; i++ {
		go RunAsync(wg, i)
	}

	wg.Wait()
	fmt.Println("DONE!!!")
}
