package learn_goroutine

import (
	"fmt"
	"sync"
	"testing"
	"time"
)

type UserBalance struct {
	sync.Mutex
	Name    string
	Balance int
}

func (u *UserBalance) Lock() {
	u.Mutex.Lock()
}

func (u *UserBalance) Unlock() {
	u.Mutex.Unlock()
}

func (u *UserBalance) Change(amount int) {
	u.Balance = u.Balance + amount
}

func Transfer(u1, u2 *UserBalance, amount int) {
	u1.Lock()
	fmt.Println("lock u1", u1.Name)
	u1.Change(-amount)

	time.Sleep(time.Second)

	u2.Lock()
	fmt.Println("lock u2", u2.Name)
	u2.Change(amount)

	time.Sleep(time.Second)

	u1.Unlock()
	u2.Unlock()
}

func TestDeadlock(t *testing.T) {
	u1 := UserBalance{
		Name:    "Ed",
		Balance: 1000000,
	}
	u2 := UserBalance{
		Name:    "Reg",
		Balance: 1000000,
	}

	go Transfer(&u1, &u2, 100000)
	go Transfer(&u2, &u1, 200000)

	time.Sleep(3 * time.Second)
	fmt.Println("u1", u1.Balance)
	fmt.Println("u2", u2.Balance)
}
